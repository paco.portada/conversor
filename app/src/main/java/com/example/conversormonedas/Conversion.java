package com.example.conversormonedas;

public class Conversion {

    private double cambio;

    public Conversion() {
        this.cambio = 0.85;
    }

    public Conversion(Double c) {
        this.cambio = c;
    }

    public String convertirADolares(String cantidad) throws NumberFormatException{
        double valor = Double.parseDouble(cantidad) / cambio;
        return Double.toString(valor);
        //return String.valueOf(valor);
    }

    public String convertirAEuros(String cantidad) throws NumberFormatException{
        double valor = Double.parseDouble(cantidad) * cambio;
        return String.format("%.2f", valor);
    }

}
