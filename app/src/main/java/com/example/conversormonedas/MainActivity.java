package com.example.conversormonedas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //private final double factorEuroDollar = 1.17;
    private final double RATIO = 0.85;
    private Conversion miConversor;

    @BindView(R.id.edEuro)
    EditText edEuro;

    @BindView(R.id.edDollar)
    EditText edDollar;

    @BindView(R.id.rdDolarEuro)
    RadioButton rdDolarEuro;

    @BindView(R.id.rdEuroDolar)
    RadioButton rdEuroDolar;

    @BindView(R.id.btConvert)
    Button btConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        btConvert.setOnClickListener(this);
        miConversor = new Conversion(RATIO);
        }

    @Override
    public void onClick(View view) {
        StringBuilder mensaje = new StringBuilder();
        boolean error = false;
        String valor;

        if (view == btConvert) {
            try {
                if (rdDolarEuro.isChecked()) {
                    valor = edDollar.getText().toString();
                    if (valor.isEmpty())
                        valor = "0";
                    edEuro.setText(miConversor.convertirAEuros(valor));
                } else {
                    valor = edEuro.getText().toString();
                    if (valor.isEmpty()) {
                        error = true;
                        mensaje.append("Debe introducir un valor");
                        edDollar.setText("");
                    } else
                        edDollar.setText(miConversor.convertirADolares(valor));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Log.e("ERROR", e.getMessage() + " /n" + e.getCause());
                error = true;
                //mensaje.append(e.getMessage());
                mensaje.append("Debe introducir un valor correcto");
            }
            if (error)
                Toast.makeText(getApplicationContext(), mensaje.toString(), Toast.LENGTH_LONG).show();
        }

    }
}
